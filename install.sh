#!/bin/ksh

WORK=work
ETC=/etc

. ./vpn.subr

VPN_EXT_IF=`load_conf ext_if vio0`
VPN_INT_NET=`load_conf int_net "10.0.0.0/16"`
VPN_EXT_IP=`ip_addr ${VPN_EXT_IF}`

VPN_PRESHARED=`load_conf l2tp_preshared helloworld`
VPN_NPPPD_USER=`load_conf npppd_user hello`
VPN_NPPPD_PASSWORD=`load_conf npppd_password world`

rm -rf ${WORK}
cp -r etc ${WORK}

FILES=`find work -type f`

set | grep 'VPN_' | while IFS='=' read key val
do
    echo "$FILES" | xargs -n1 -I{} sed -i "s^${key}^${val}^g" {}
done

echo "${FILES}" | while read file
do
    target=/etc${file#work*}
    install -m 600 $file $target
done

# start daemons
rcctl enable npppd isakmpd
rcctl restart npppd isakmpd

# enable ipsecctl
sed -i 's/ipsec=NO/ipsec=YES/' /etc/rc.conf
ipsecctl -f /etc/ipsec.conf

# install pf rules
pf_installed=`grep pf.conf.vpn /etc/pf.conf`
if [ $? -ne 0 ] ; then
	echo 'include "/etc/pf.conf.vpn"' >> /etc/pf.conf
fi

# enable pf
pfctl -e
pfctl -f /etc/pf.conf

# enable packet forwarding
sysctl net.inet.ip.forwarding=1
echo 'net.inet.ip.forwarding=1' >> /etc/sysctl.conf
