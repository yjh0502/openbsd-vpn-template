#!/bin/ksh

. ./vpn.subr

EXT_IF=`load_conf ext_if vio0`
EXT_IP=`ip_addr ${EXT_IF}`

EMAIL=`load_conf cf_email`
KEY=`load_conf cf_key`
ZONE=`load_conf cf_zone`
RECORD=`load_conf cf_record`
DOMAIN=`load_conf cf_domain`

DATA='{"type":"A","name":"'${DOMAIN}'","content":"'${EXT_IP}'","ttl":120,"proxied":false}'

curl -X PUT "https://api.cloudflare.com/client/v4/zones/${ZONE}/dns_records/${RECORD}" \
     -H "X-Auth-Email: $EMAIL" \
     -H "X-Auth-Key: $KEY" \
     -H "Content-Type: application/json" \
     --data "${DATA}"
