# openbsd-vpn-template

## Config
update `vpn.conf` file to change PSK/username/passwords.

## Install

```sh
# install configuration files, start daemons, update pf rules
sh install.sh
```

## Update DNS record

Support cloudflare v4 API to update DNS

```sh
cf_email=example@example.com \
cf_key=xxx \
cf_zone=xxx \
cf_record=xxx \
cf_domain=example.com \
cf.sh
```

## Tested clients

 - macOS sierra (10.12.6)
 - Windows 10
